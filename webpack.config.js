var path = require('path');

const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	entry: './src/app.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'public')
	},
	watch: true,
	module: {
		loaders: [
			{
				test:/\.js$/,
				exclude:/node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015', 'stage-1']
				}
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('css-loader', 'sass-loader')
			}
		],
		
	},
	plugins: [
		new ExtractTextPlugin({
			filename: 'style.css',
			allChunks: true,
		})
	]
};