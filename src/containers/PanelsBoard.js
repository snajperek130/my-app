import React from "react";
import {Row} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {addPanel, deletePanel} from "../actions/panelActions";
import Panel from "../components/Panel";
import fetch from 'isomorphic-fetch'
import ContainerColour from "../components/ContainerColors";
import stateOfTask from "../type/StateOfTask";
import moment from "moment";

class PanelsBoard extends React.Component {
	
	componentDidMount() {
		fetch('/api/users').then(res => res.json()).then(data => console.log(data));
		this.updateDimensions();
		window.addEventListener("resize", this.updateDimensions.bind(this));
	}
	
	componentWillUnmount() {
		this.updateDimensions();
		window.removeEventListener("resize", this.updateDimensions.bind(this));
	}
	
	componentDidUpdate() {
		this.updateDimensions();
	}

// !! CHECK IT
//https://osvaldas.info/responsive-equal-height-blocks
	updateDimensions() {
		const windowWidth = window.innerWidth;
		const panelElements = document.getElementsByClassName("panel").length;
		const panelElement = document.getElementsByClassName("panel")[0];
		if (panelElement) {
			const marginElement = parseFloat(document.defaultView.getComputedStyle(document.getElementsByClassName("panel")[0], null)["margin-right"]);
			const count = parseInt(Math.floor(windowWidth / (panelElement.clientWidth + (marginElement * 2))), 10);
			let sum;
			if (panelElements <= count) {
				sum = windowWidth - ((panelElement.clientWidth * panelElements) + ((marginElement * 2) * (panelElements - 1)));
			}
			else {
				sum = windowWidth - ((panelElement.clientWidth * count) + ((marginElement * 2) * (count - 1)));
			}
			let heh = this.heh;
			if ((sum / 2) - 25 > 0) {
				heh.style.marginLeft = (sum / 2) - 25 + "px";
			}
		}
	}
	
	render() {
		return (
			<div>
				<Row className="center container-title">
					<div>{moment().format("dddd, MMMM Do YYYY")}</div>
				</Row>
				<Row>
					<ul className="container fixed-container">
						<div className="heh" ref={(heh) => this.heh = heh}>
							{this.getPanels()}
						</div>
					</ul>
				</Row>
				<Row className="center">
					<button onClick={this.addPanel}
					        className="btn btn-success btn-lg">Add Panel
					</button>
				</Row>
				<Row className="center container-colour">
					<ContainerColour/>
				</Row>
			</div>
		);
	}
	
	getPanels() {
		return this.props.panels.map((square) =>
			<Panel key={square._id}
			       id={square._id}
			       title={square.title}
			       body={square.body}
			       color={square.color}
			       onClick={this.deleteOrNavigatePanel}
			/>
		);
	}
	
	addPanel = () => {
		let panel = {
			_id: 10,
			title: "new",
			body: "new Body",
			color: stateOfTask.toDo
		};
		
		this.props.addPanel(panel);
	};

    deleteOrNavigatePanel = (data) => {
        if (data.type === "deletePanel") {
            this.props.deletePanel(data.id);
        }
        else {
            this.props.history.push('/edit-panel/' + data.id)
        }
    };
}

function mapStateToProps(state) {
	return {
		panels: state.panels.panels
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		deletePanel: deletePanel,
		addPanel: addPanel
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PanelsBoard);
