import React from "react";
import {Col} from "react-bootstrap";
import {getPanelById} from "../actions/panelActions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class EditPanel extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {name: ""};
		this.handleTitleChange = this.handleTitleChange.bind(this);
		this.handleBodyChange = this.handleBodyChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	
	handleTitleChange(event) {
		// this.setState({value: event.target.panel.title});
	}
	handleBodyChange(event) {
		// this.setState({value: event.target.panel.title});
	}
	
	handleSubmit(event) {
		event.preventDefault();
	}
	
	
	componentDidMount() {
		this.props.getPanelById(parseInt(this.props.match.params.id, 10));
	}
	
	renderPanel() {
		if (this.props.panel) {
			return (
				<div className="container">
					<form className="form-horizontal" role="form" method="post" onSubmit={this.handleSubmit}>
						<div className="form-group">
							<label className="col-sm-2 control-label">Title</label>
							<div className="col-sm-10">
								<input type="text" className="form-control" id="name" name="name"
								       placeholder="First & Last Name" value={this.props.panel.title} onChange={this.handleTitleChange} />
							</div>
						</div>
						<div className="form-group">
							<label className="col-sm-2 control-label">Body</label>
							<div className="col-sm-10">
								<textarea className="form-control" rows="4" name="message"
								          value={this.props.panel.body} onChange={this.handleBodyChange}></textarea>
							</div>
						</div>
						<div className="form-group">
							<div className="col-sm-10 col-sm-offset-2">
								<input id="submit" name="submit" type="submit" value="Send"
								       className="btn btn-primary"/>
							</div>
						</div>
					</form>
				</div>
			);
		}
	}
	
	render() {
		return (
			<Col onClick={() => this.deleteOrNavigatePanel()} md={2} sm={4} xs={6}>
				{this.renderPanel()}
			</Col>
		);
	}
}

function mapStateToProps(state) {
	return {
		panel: state.panels.panel
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		getPanelById: getPanelById
	}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(EditPanel);
