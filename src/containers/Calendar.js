import React from "react";
import DragAndDropCalendar from "react-big-calendar";
import moment from "moment";
import {connect} from "react-redux";
import BigCalendar from "react-big-calendar";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import {updateCalendarEvent} from "../actions/calendarActions";
import {bindActionCreators} from "redux";

const DragAndDropCalendarContainer = withDragAndDrop(BigCalendar);
DragAndDropCalendar.setLocalizer(
	DragAndDropCalendar.momentLocalizer(moment)
);

class Calendar extends React.Component {
	
	constructor() {
		super();
		this.moveEvent = this.moveEvent.bind(this);
		this.addElementsHoverListener();
	}
	
	render() {
		return (<div className="calendar-contgainer">
			<DragAndDropCalendarContainer
				{...this.props}
				selectable
				culture='en-GB'
				onEventDrop={this.moveEvent}
				defaultView='week'
				events={this.props.events}
				defaultDate={new Date()}
				onSelectEvent={this.navigateToEditPanel}
			/>
		</div>);
	}
	
	moveEvent({ event, start, end }) {
		this.props.updateCalendarEvent({ event, start, end });
	}
	
	addElementsHoverListener() {
		const self = this;
		document.addEventListener("DOMContentLoaded", function(){
			self.addElementsHover();
		}, false);
	}
	
	addElementsHover () {
		let events = [...document.getElementsByClassName("rbc-event")];
		console.log(events);
		if(events.length !== 0) {
			events.forEach(event => {
				event.addEventListener("mouseover", this.mouseOver);
			});
		}
	}
	
	navigateToEditPanel(e) {
		this.props.history.push("/edit-panel/"+e.id);
	}
	
	mouseOver(e) {
		if(e.path[1].className === "rbc-event") {
			const x = e.path[1].offsetTop;
			const y = e.path[1].offsetLeft;
			const width = e.path[1].clientWidth;
		}
	}
}

function mapStateToProps(state) {
	return {
		events: state.events.events
	};
}

function dispatchStateToProps(dispatch) {
	return bindActionCreators({
		updateCalendarEvent: updateCalendarEvent
	}, dispatch);
}

export default connect(mapStateToProps, dispatchStateToProps)(Calendar);