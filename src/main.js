import React from "react";
import "./assets/style.scss";
import "./assets/calendar.scss";
import {Route, Switch} from "react-router-dom";
import EditPanel from "./containers/EditPanel";
import PanelsBoard from "./containers/PanelsBoard";
import Calendar from "./containers/Calendar";
import Watcher from "./containers/Watcher";
import Bin from "./containers/Bin";

const Main = () => (
	<main>
		<Switch>
			<Route exact path='/' render={props => <PanelsBoard {...props} />} />}
			<Route path='/edit-panel/:id' render={props => <EditPanel {...props} />} />}
			<Route path='/calendar' render={props => <Calendar {...props} />} />}
			<Route path='/watcher' render={props => <Watcher {...props} />} />}
			<Route path='/bin' render={props => <Bin {...props} />} />}
		</Switch>
	</main>
);

export default Main;