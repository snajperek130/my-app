import React from "react";
import {render} from "react-dom";
import {BrowserRouter} from "react-router-dom";
import App from "./app";
import {applyMiddleware, combineReducers, createStore} from "redux";
import {logger} from "redux-logger";
import {PanelReducers} from "./reducers/PanelReducer";
import Provider from "react-redux/es/components/Provider";
import {CalendarReducers} from "./reducers/CalendarReducer";

const middleware = applyMiddleware(logger);
const rootReducer = combineReducers({
	panels: PanelReducers,
	events: CalendarReducers
});

const store = createStore(rootReducer, middleware);

render((
	<Provider store={store}>
		<BrowserRouter>
			<App/>
		</BrowserRouter>
	</Provider>
), document.getElementById("app"));
