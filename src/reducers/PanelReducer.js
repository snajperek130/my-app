/* eslint-disable no-case-declarations */
"use script";
import panels from "../data/panels";

export function PanelReducers(state = {panels: panels}, action = {type: "initial"}) {
	switch (action.type) {
	case "ADD_PANEL":
		return {
			panels: [...state.panels, action.payload]
		};
	case "DELETE_PANEL":
		const panelArr = state.panels;
		const indexToDelete = panelArr.findIndex((panel) => {
			return panel._id === action.payload;
		});
		const newPanels = [...state.panels];
		const panels = [...newPanels.slice(0, indexToDelete), ...newPanels.slice(indexToDelete + 1)];
		return {
			panels: [...panels]
		};
		
	case "UPDATE_PANEL":
		const currentPanelToUpdate = [...state.panels];
		const indexToUpdate = currentPanelToUpdate.findIndex((panel) => {
			return panel._id === action._id;
		});
			
		const updatedPanel = {
			...currentPanelToUpdate[indexToUpdate]
		};
			
			// let panelUpdate = [...currentPanelToUpdate.slice(0, indexToUpdate), newPanelToUpdate,
			// 	...currentPanelToUpdate.slice(indexToUpdate + 1)];
		return {
			panels: "ds"
		};
	case "GET_PANEL_BY_ID":
		let panel =  state.panels.find(panel => {
			return panel._id === action.payload;
		});
		return {
			panels: [...state.panels],
			panel: panel
		};
	default:
	}
	
	
	return state;
}
