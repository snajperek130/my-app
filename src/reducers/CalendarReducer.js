/* eslint-disable no-case-declarations */
"use script";
import events from "../data/events";

export function CalendarReducers(state = {events: events}, action = {type: "initial"}) {
	switch (action.type) {
	case "ADD_CALENDAR_EVENT":
		// return {
		// 	panels: [...state.panels, action.payload]
		// };
	case "DELETE_CALENDAR_EVENT":
		// const panelArr = state.panels;
		// const indexToDelete = panelArr.findIndex((panel) => {
		// 	return panel._id === action.payload;
		// });
		// const newPanels = [...state.panels];
		// const panels = [...newPanels.slice(0, indexToDelete),
		// ...newPanels.slice(indexToDelete + 1)]; return { panels: [...panels]
		// };
	case "UPDATE_CALENDAR_EVENT":
		const {events} = state;
		const {start, end, event} = action.payload;
			
		const idx = events.indexOf(event);
		const updatedEvent = {...event, start, end};
			
		const eventsRef = [...events];
		const nextEvents = [...eventsRef.slice(0, idx), updatedEvent, ...eventsRef.slice(idx + 1)];
			
		return {
			events: [...nextEvents]
		};
		
	default:
	}
	
	return state;
}
