import React, {Component} from "react";
import Main from "./main";
import Menu from "./components/NavigationBar";
import {Col, Row} from "react-bootstrap";

class App extends Component {
	
	render() {
		return (
			<div>
				<Row>
					<Menu/>
				</Row>
				
				<Row className="main-body">
					<Main/>
				</Row>
				
				<Row className="footer">
					<Col md={12}>
						<div className="container">
							{/*<Footer/>*/}
						</div>
					</Col>
				</Row>
			</div>
		);
	}
}

export default App;
