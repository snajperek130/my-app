"use script"

export function addPanel(panel) {
	return {
		type: "ADD_PANEL",
		payload: panel
	};
}

export function deletePanel(_id) {
	return {
		type: "DELETE_PANEL",
		payload: _id
	};
}

export function updatePanel(panel) {
	return {
		type: "UPDATE_PANEL",
		payload: panel
	};
}

export function getPanelById(_id) {
	return {
		type: "GET_PANEL_BY_ID",
		payload: _id
	};
}