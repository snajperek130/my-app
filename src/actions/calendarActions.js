"use script"

export function updateCalendarEvent(payload) {
	return {
		type: "UPDATE_CALENDAR_EVENT",
		payload: payload
	};
}