import * as React from "react";
import StateOfTask from "../type/StateOfTask";

const ContainerColour = () => {
	return (
		<div>
			<p>Colors</p>
			<div className="container">
				<h2>Basic Table</h2>
				<p>The .table class adds basic styling (light padding and only
					horizontal dividers) to a table:</p>
				<table className="table">
					<thead>
						<tr>
							<th>Status</th>
							<th>Color</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>John</td>
							<td>Doe</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	);
};

export default ContainerColour;