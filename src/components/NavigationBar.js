import React from "react";
import {Navbar, NavItem, Nav, Badge} from "react-bootstrap";

class NavigationBar extends React.Component {
	render() {
		return (
			<Navbar inverse fixedTop>
				<Navbar.Header>
					<Navbar.Brand>
						<a href="/">Today</a>
					</Navbar.Brand>
					<Navbar.Toggle/>
				</Navbar.Header>
				<Navbar.Collapse>
					<Nav>
						<NavItem eventKey={1} href="/calendar">Calendar</NavItem>
						<NavItem eventKey={2} href="/watcher">Watcher Settings</NavItem>
						<NavItem eventKey={3} href="/statistics">Statistics</NavItem>
						<NavItem eventKey={4} href="/bin">Bin</NavItem>
					</Nav>
					<Nav pullRight>
						<NavItem eventKey={5} href="/">Login/Logout</NavItem>
						{/*<NavItem eventKey={2} href="/logout">Your cart <Badge className="badge">1</Badge></NavItem>*/}
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		);
	}
}

export default NavigationBar;