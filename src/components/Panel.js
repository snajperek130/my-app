import React from "react";
import {Col} from "react-bootstrap";

class Panel extends React.Component {
	
	constructor() {
		super();
	}
	
	render() {
		return (
			<Col onClick={() => this.deleteOrNavigatePanel()} md={2} sm={4} xs={6}
			     className="panel" style={{backgroundColor: `${this.props.color}`}}>
				<div className="panel-title">{this.props.title}</div>
				<i onClick={this.deleteOrNavigatePanel}
				   className="close-icon glyphicon glyphicon-remove-sign"></i>
				<div className="panel-body">{this.props.body}</div>
			</Col>
		)
	}
	
	deleteOrNavigatePanel = (e) => {
		const data = {id: this.props.id, type: "goToPanel"}
		if(e) {
			e.stopPropagation();
			data.type="deletePanel";
		}
		this.props.onClick(data);
	}
}

export default Panel;
