export default [
	{
		"id": 1,
		"title": "title 1",
		"start": new Date(2017, 9, 5, 16, 0, 0, 0),
		"end": new Date(2017, 9, 5, 18, 0, 0, 0),
	},
	{
		"id": 2,
		"title": "title 2",
		"start": new Date(2017, 9, 5, 9, 0, 0, 0),
		"end": new Date(2017, 9, 5, 12, 0, 0, 0),
	},
	{
		"id": 3,
		"title": "title 3",
		"start": new Date(2017, 9, 5, 9, 0, 0, 0),
		"end": new Date(2017, 9, 5, 12, 0, 0, 0),
	},
];