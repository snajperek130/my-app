import stateOfTask from "../type/StateOfTask";

export default [
	{_id: 1, title: "title 1", body: "bodyyyy", color: stateOfTask.warning},
	{_id: 2, title: "title 2", body: "bodyyyedqawsdey", color: stateOfTask.toDo},
	{_id: 3, title: "title 3", body: "bodyyyfdasfasdgasy", color: stateOfTask.completed}
];